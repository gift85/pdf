<?php

namespace App\Controllers;


class Create extends MainController
{
    public function default()
    {
        return $this->render('new.html.twig', array('message' => 'hello'));
    }

    public function upload()
    {
        if (empty($_FILES['file'])) {
            return $this->render('new.html.twig', array('message' => 'no file'));
        }

        if (mime_content_type($_FILES['file']['tmp_name']) !== 'application/pdf') {
            return $this->render('new.html.twig', array('message' => 'wrong file'));
        }

        $uploads_dir = __DIR__ . '/../../public';

        move_uploaded_file($_FILES['file']['tmp_name'], $uploads_dir . '/' . basename($_FILES['file']['name']));

    }
}