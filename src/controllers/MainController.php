<?php

namespace App\Controllers;

use Twig_Environment;

class MainController
{
    protected $templater;

    public function __construct(Twig_Environment $templater)
    {
        $this->templater = $templater;
    }

    public function render(string $templateName, array $data = array())
    {
        return $this->templater->render($templateName, $data);
    }
}