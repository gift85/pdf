<?php

namespace App\Controllers;

class Index extends MainController
{

    public function default()
    {
        return $this->render('index.html.twig', array('message' => 'hello'));
    }
}